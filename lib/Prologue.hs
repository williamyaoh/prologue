-- |
-- Module      : Prologue
-- Description : Modified Prelude; partial functions out, useful types/functions in.
-- Copyright   : (c) 2018 William Yao
-- License     : BSD3
-- Maintainer  : William Yao <williamyaoh@gmail.com>
-- Stability   : experimental
-- Portabilty  : portable
--
-- In particular, some notable differences from the default Prelude:
--
-- * `MonadIO' and `liftIO' are exported by default
-- * `Bifunctor' and the functions to map over them are exported
-- * No list functions are exported, and no partial functions
-- * "Formatting" functions are exported, since string manipulation comes up a lot
-- * "Control.Lens" accessing operators are exported
-- * "Data.Default" is exported
-- * "Data.These" is exported
-- * Fixed-width integers from "Data.Word" and "Data.Int" are exported using
--   more concise type synonyms `I32', `U32', etc., and `Float' and `Double'
--   have type synonyms `F32' and `F64'.

module Prologue
  ( id, const
  , (.), (.-), ($), (&), flip, on
  , until, asTypeOf
  , error, undefined
  , seq, ($!)
  , Bool(..), (&&), (||), not, otherwise
  , Maybe(..), maybe
  , Either(..), either
  , These(..), these, fromThese, mergeThese
  , Ordering(..)
  , Char
  , Text, pack, unpack
  , fst, snd
  , curry, uncurry
  , Eq, (==), (/=)
  , Ord, compare, (<), (<=), (>), (>=), max, min
  , Enum, succ, pred
  , Bounded, minBound, maxBound
  -- * Numeric types
  , Int, Integer, Word, Float, Double, Rational
  , Num, (+), (*), abs, signum, fromInteger, negate, (-)
  , Real, toRational
  , Integral, quot, rem, div, mod, quotRem, divMod, toInteger
  , Fractional, (/), recip, fromRational
  , Floating, pi, exp, log, sin, cos, asin, acos, sinh, cosh, asinh, acosh, atanh
  , RealFrac, properFraction, truncate, round, ceiling, floor
  , RealFloat, floatRadix, floatDigits, floatRange, decodeFloat, encodeFloat, exponent
  , significand, scaleFloat, isNaN, isInfinite, isDenormalized, isNegativeZero, isIEEE, atan2
  , subtract, even, odd, gcd, lcm, (^), (^^), fromIntegral, realToFrac
  -- ** Fixed-width numerical types
  , I8, I16, I32, I64
  , U8, U16, U32, U64
  , F32, F64
  -- ** Category-theoretical stuff
  , Semigroup, (<>)
  , Monoid, mempty, mappend, mconcat
  , Functor, fmap, (<$), (<$>)
  , Bifunctor, bimap, first, second
  , Applicative, pure, (<*>), (*>), (<*), ($>>), ($$>>)
  , Alternative, aempty, (<|>)
  , Monad, return, (>>=), (>>), (>=>), join
  , MonadFail, fail
  , MonadIO, liftIO
  , mapM_, sequence_, (=<<)
  , Foldable, foldMap, foldr, foldl, elem
  , Traversable, traverse, sequenceA, mapM, sequence
  , null, length
  , and, or, any, all
  , ShowS, Show, showsPrec, show, showList
  , shows, showChar, showParen
  , IO, FilePath, IOError, ioError, userError
  -- * Nonempty lists
  , NonEmpty(..)
  -- * Data defaults
  , Default, def
  -- * Lens operators
  , view, set, over, (^.), (.~), (%~)
  -- * Text formatting
  , Format, format, format', formatToString, (%), fprint, hprint
  , t, t', d, b, o, x, sh, c, f, sf
  -- * Raw strings
  , r
  )
where

import Prelude hiding ( fail )

import Data.Bifunctor
import Data.Default
import Data.Function
import Data.Int
import Data.List.NonEmpty ( NonEmpty(..) )
import Data.Semigroup
import Data.Text          hiding ( all, any, foldl, foldr, length, null )
import Data.These
import Data.Word

import Control.Applicative    as App
import Control.Lens
import Control.Monad          hiding ( fail )
import Control.Monad.Fail
import Control.Monad.IO.Class

import Formatting
import Formatting.ShortFormatters hiding ( r )

import Text.RawString.QQ

type I8 = Int8
type I16 = Int16
type I32 = Int32
type I64 = Int64

type U8 = Word8
type U16 = Word16
type U32 = Word32
type U64 = Word64

type F32 = Float
type F64 = Double

-- | Forward composition (`(.)' with the arguments reversed).
(.-) :: (a -> b) -> (b -> c) -> (a -> c)
(.-) = flip (.)

-- | Alternative name for Alternative `Control.Applicative.empty', to
--   avoid clashing with data structures.
aempty :: Alternative f => f a
aempty = App.empty

-- | Alternative name for `sformat', to stick to convention.
format' :: Format Text a -> a
format' = sformat

-- | Alternative name for `st', to stick to convention.
t' :: Format r (Text -> r)
t' = st

infixl 2 $>>
-- | Flipped `fmap', so that we can write functor/applicative-based code
--   in serial order.
--
--   > foo :: Maybe Int
--   > foo = bar $>> \x -> x * 2
($>>) :: Functor f => f a -> (a -> b) -> f b
($>>) = flip fmap

infixl 2 $$>>
-- | A version of `liftA2' that puts the function at the end rather than as
--   first parameter, so that we can write applicative-based code in serial
--   order. Meant to be used with `(&)'.
--
--   Instead of:
--
--   > foo :: Maybe String
--   > foo = do
--   >   x <- bar
--   >   y <- baz
--   >   pure $ show x ++ y
--
--   We can write:
--
--   > foo :: Maybe String
--   > foo = bar & baz $$>> \x y ->
--   >   show x ++ y
--
--   which is both shorter and (depending on the applicative in question)
--   might be more efficient, through using the applicative functions rather
--   than the monadic ones.
($$>>) :: Applicative f => f b -> (a -> b -> c) -> f a -> f c
($$>>) b f a = liftA2 f a b
