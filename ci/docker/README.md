# prologue/ci/docker

Build specification with most of `prologue`'s transitive dependencies
pre-installed. It should also install any extra build tools, like
`stylish-haskell` or `hlint`, that are needed for the CI process. If you want to
add an extra tool like that, add it here. This is the canonical build image.

Run `sudo docker build .` in this directory to build an image.
