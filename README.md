# prologue

My custom Haskell Prelude.

Some notable differences:

* `MonadIO` and `liftIO` are exported by default
* `Bifunctor` and the functions to map over them are exported
* No list functions are exported, and no partial functions
* `Formatting` functions are exported, since string manipulation comes up a lot
* `Control.Lens` accessing operators are exported
* `Data.Default` is exported
* `Data.These` is exported
* Fixed-width integers from `Data.Word` and `Data.Int` are exported using
  more concise type synonyms `I32`, `U32`, etc., and `Float` and `Double`
  have type synonyms `F32` and `F64`.
